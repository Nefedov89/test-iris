<?php

declare(strict_types = 1);

namespace Task\Iris\Traits;

use Exception;
use PDO;
use Task\Iris\DB\DBConnection;

/**
 * Trait DBHelper
 * @package Task\Iris\Traits
 */
trait DBHelper
{
    /**
     * Get PDO connection.
     *
     * @return PDO
     */
    public static function getPDO(): PDO
    {
        return DBConnection::getInstance()->getConnection();
    }

    /**
     * Check if table exists.
     *
     * @param string $table
     *
     * @return bool
     */
    public static function checkIfTableExists(string $table): bool
    {
        try {
            $result = static::getPDO()
                ->query("SHOW TABLES LIKE '{$table}'")
                ->rowCount();

        } catch (Exception $e) {
            die($e->getMessage());
        }

        return $result > 0;
    }
}