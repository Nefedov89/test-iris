<?php

declare(strict_types = 1);

namespace Task\Iris;

use Exception;
use Task\Iris\DB\DBInitialization;
use Task\Iris\Traits\DBHelper;

/**
 * Class ReportHandler
 */
class ReportHandler
{
    use DBHelper;

    const FILE_PATH = './data/report.csv';

    public static $dataForDb = [
        'merchants'    => [],
        'batches'      => [],
        'transactions' => [],
    ];

    /**
     * Parse csv report file.
     *
     * @return void
     *
     * @throws Exception
     */
    public static function parseReport(): void
    {
        try {
            $handle = fopen(static::FILE_PATH, 'r');
            $fieldsMap = ReportFields::getFieldsMap();

            if ($handle !== false) {
                $row = 0;
                $fields = [];

                while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                    $row++;

                    if ($row === 1) {
                        $fields = $data;

                        foreach ($fieldsMap as $machineName => $readableName) {
                            if (!in_array($readableName, $fields)) {
                                throw new Exception('Report file doesn\'t contain necessary field: ' . $readableName);
                            }
                        }
                        continue;
                    }

                    $dataForDb = [];

                    foreach ($fieldsMap as $machineName => $readableName) {
                        $dataForDb[$machineName] = $data[array_search($readableName, $fields)];
                    }

                    // Collect data for db.
                    static::collectDataForQuery($dataForDb);
                }

                fclose($handle);

                // Save to DB.
                // TODO: split it into chunks or use queue server.
                static::saveDataToDb();
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Collect data for db.
     *
     * @param $data
     *
     * @return void
     */
    public static function collectDataForQuery($data): void
    {
        foreach (DBInitialization::TABLES as $table) {
            switch ($table) {
                // Merchants.
                case 'merchants':
                    if (!isset(static::$dataForDb[$table][$data['merchants_id']])) {
                        static::$dataForDb[$table][$data['merchants_id']] = [
                            'id'   => $data['merchants_id'],
                            'name' => $data['merchants_name'],
                        ];
                    }

                    break;

                // Batches.
                case 'batches':
                    if (!isset(static::$dataForDb[$table][$data['batches_ref_num']])) {
                        static::$dataForDb[$table][$data['batches_ref_num']] = [
                            'ref_num' => $data['batches_ref_num'],
                            'date'    => $data['batches_date'],
                            'm_id'    => $data['merchants_id'],
                        ];
                    }
                    break;

                // Transactions.
                case 'transactions':
                    static::$dataForDb[$table][] = [
                        'date'        => $data['transactions_date'],
                        'type'        => $data['transactions_type'],
                        'card_type'   => $data['transactions_card_type'],
                        'card_number' => $data['transactions_card_number'],
                        'amount'      => $data['transactions_amount'],
                        'b_ref_num'   => $data['batches_ref_num'],
                    ];
                    break;
            }
        }
    }

    /**
     * Save data to DB.
     *
     * @return void
     */
    private static function saveDataToDb(): void
    {
        try {
            foreach (DBInitialization::TABLES as $table) {
                $methodName = 'save' . ucfirst($table);

                if (method_exists(get_called_class(), $methodName)) {
                    call_user_func(get_called_class() . "::{$methodName}");
                }
            }
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Save merchants.
     *
     * @throws Exception
     */
    public static function saveMerchants(): void
    {
        if (!isset(static::$dataForDb['merchants'])) {
            throw new Exception('Error. No parsed merchants');
        }

        /** @var \PDO $pdo */
        $pdo = static::getPDO();

        $questionMarks = [];
        $values = [];

        foreach (static::$dataForDb['merchants'] as $merchant) {
            $questionMarks[] = '(?, ?)';

            foreach (['id', 'name'] as $field) {
                $values[] = $merchant[$field];
            }
        }

        $pdo->prepare("INSERT INTO merchants(id, `name`) VALUES " . implode(', ', $questionMarks))
            ->execute($values);
    }

    /**
     * Save batches.
     *
     * @throws Exception
     */
    public static function saveBatches(): void
    {
        if (!isset(static::$dataForDb['batches'])) {
            throw new Exception('Error. No parsed batches');
        }

        /** @var \PDO $pdo */
        $pdo = static::getPDO();

        $questionMarks = [];
        $values = [];

        foreach (static::$dataForDb['batches'] as $batch) {
            $questionMarks[] = '(?, ?, ?)';

            foreach (['ref_num', 'date', 'm_id'] as $field) {
                $values[] = $batch[$field];
            }
        }

        $pdo->prepare("INSERT INTO batches(ref_num, `date`, m_id) VALUES " . implode(', ', $questionMarks))
            ->execute($values);
    }

    /**
     * Save transactions.
     *
     * @throws Exception
     */
    public static function saveTransactions(): void
    {
        if (!isset(static::$dataForDb['transactions'])) {
            throw new Exception('Error. No parsed transactions');
        }

        /** @var \PDO $pdo */
        $pdo = static::getPDO();

        $questionMarks = [];
        $values = [];

        foreach (static::$dataForDb['transactions'] as $transaction) {
            $questionMarks[] = '(?, ?, ?, ?, ?, ?)';

            foreach (['date', 'type', 'card_type', 'card_number', 'amount', 'b_ref_num'] as $field) {
                $values[] = $transaction[$field];
            }
        }

        $pdo->prepare("INSERT INTO transactions(`date`, `type`, card_type, card_number, amount, b_ref_num) VALUES " . implode(', ', $questionMarks))
            ->execute($values);
    }
}