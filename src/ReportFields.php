<?php

declare(strict_types = 1);

namespace Task\Iris;

/**
 * Class ReportFields
 */
class ReportFields
{
    const MERCHANT_ID             = 'merchants_id';
    const MERCHANT_NAME           = 'merchants_name';
    const BATCH_DATE              = 'batches_date';
    const BATCH_REF_NUM           = 'batches_ref_num';
    const TRANSACTION_DATE        = 'transactions_date';
    const TRANSACTION_TYPE        = 'transactions_type';
    const TRANSACTION_CARD_TYPE   = 'transactions_card_type';
    const TRANSACTION_CARD_NUMBER = 'transactions_card_number';
    const TRANSACTION_AMOUNT      = 'transactions_amount';

    /**
     * Get fields map.
     *
     * @return array
     */
    public static function getFieldsMap(): array
    {
        return  [
            ReportFields::MERCHANT_ID             => 'Merchant ID',
            ReportFields::MERCHANT_NAME           => 'Merchant Name',
            ReportFields::BATCH_DATE              => 'Batch Date',
            ReportFields::BATCH_REF_NUM           => 'Batch Reference Number',
            ReportFields::TRANSACTION_DATE        => 'Transaction Date',
            ReportFields::TRANSACTION_TYPE        => 'Transaction Type',
            ReportFields::TRANSACTION_CARD_TYPE   => 'Transaction Card Type',
            ReportFields::TRANSACTION_CARD_NUMBER => 'Transaction Card Number',
            ReportFields::TRANSACTION_AMOUNT      => 'Transaction Amount'
        ];
    }
}