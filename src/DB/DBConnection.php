<?php

declare(strict_types = 1);

namespace Task\Iris\DB;

use Exception;
use PDO;

/**
 * Class DBConnection
 * @package Task\Iris\DB
 */
class DBConnection
{
    /**
     * @var DBConnection
     */
    private static $instance;

    /**
     * @var string
     */
    private $host = 'localhost';

    /**
     * @var string
     */
    private $dbUser = 'root';

    /**
     * @var string
     */
    private $dbPass = 'pass';

    /**
     * @var string
     */
    private $dbName = 'task_iris';

    /**
     * @var PDO
     */
    private $connection;

    /**
     * Get DB instance.
     *
     * @return DBConnection
     */
    public static function getInstance(): DBConnection
    {
        if (empty(static::$instance)) {
            static::$instance = new DBConnection();
        }

        return static::$instance;
    }

    /**
     * Get PDO connection.
     *
     * @return PDO
     */
    public function getConnection(): PDO
    {
        return $this->connection;
    }

    /**
     * Database constructor.
     */
    private function __construct()
    {
        try {
            $this->connection = new PDO(
                "mysql:host={$this->host};dbname={$this->dbName}",
                $this->dbUser,
                $this->dbPass,
                [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]
            );
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Magic method clone.
     */
    private function __clone() {}
}