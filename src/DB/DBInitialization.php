<?php

declare(strict_types = 1);

namespace Task\Iris\DB;

use Exception;
use Task\Iris\Traits\DBHelper;

/**
 * Class DBInitialization
 * @package Task\Iris\DB
 */
class DBInitialization
{
    use DBHelper;

    const TABLES = [
        'merchants',
        'batches',
        'transactions',
    ];

    /**
     * Init database.
     *
     * @return void
     */
    public static function initDB(): void
    {
        foreach (static::TABLES as $table) {
            $methodName = 'create' . ucfirst($table) . 'Table';

            if (method_exists(get_called_class(), $methodName)) {
                call_user_func(get_called_class() . "::{$methodName}");
            }
        }
    }

    /**
     * Create merchant table.
     *
     * @return void
     */
    public static function createMerchantsTable(): void
    {
        try {
            $sql = <<<EOSQL
              CREATE TABLE IF NOT EXISTS merchants(
                  id NUMERIC(18, 0) UNSIGNED NOT NULL,
                  `name` varchar(100) NOT NULL,
                  PRIMARY KEY (id)
              );
EOSQL;
            static::getPDO()->exec($sql);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Create batches table.
     *
     * @return void
     */
    public static function createBatchesTable(): void
    {
        try {
            $sql = <<<EOSQL
              CREATE TABLE IF NOT EXISTS batches(
                  ref_num NUMERIC(24, 0) UNSIGNED NOT NULL,
                  `date` DATE NOT NULL,
                  m_id NUMERIC(18, 0) UNSIGNED NOT NULL,
                  PRIMARY KEY (ref_num),
                  FOREIGN KEY (m_id) REFERENCES merchants(id) 
              );
EOSQL;
            static::getPDO()->exec($sql);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * Create batches table.
     *
     * @return void
     */
    public static function createTransactionsTable(): void
    {
        try {
            $sql = <<<EOSQL
              CREATE TABLE IF NOT EXISTS transactions(
                  `date` DATE NOT NULL,
                  `type` VARCHAR (20) NOT NULL,
                  card_type VARCHAR (2) NOT NULL,
                  card_number VARCHAR (20) NOT NULL,
                  amount FLOAT NOT NULL,
                  b_ref_num NUMERIC(24, 0) UNSIGNED NOT NULL,
                  FOREIGN KEY (b_ref_num) REFERENCES batches(ref_num) 
              );
EOSQL;
            static::getPDO()->exec($sql);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
}
