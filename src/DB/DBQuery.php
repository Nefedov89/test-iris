<?php

declare(strict_types = 1);

namespace Task\Iris\DB;

use PDO;
use Task\Iris\Traits\DBHelper;

/**
 * Class DBQuery
 * @package Task\Iris\DB
 */
class DBQuery
{
    use DBHelper;

    /**
     * Get all transactions for a batch.
     *
     * @param string $merchantId
     * @param string $trDate
     * @param string $refNum
     *
     * @return array
     */
    public function getAllTransactionsForBatch(string $merchantId, string $trDate, string $refNum): array
    {
        $sql = 'SELECT tr.date, tr.type, tr.card_type, tr.card_number, tr.amount
                  FROM transactions AS tr
                  JOIN batches b ON tr.b_ref_num = b.ref_num
                  JOIN merchants m ON b.m_id = m.id
                  WHERE m.id = ? AND tr.date = ? AND b.ref_num = ?';
        /** @var PDO $stmt */
        $stmt = static::getPDO()->prepare($sql);
        $stmt->execute([$merchantId, $trDate, $refNum]);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Get stats for a batch.
     *
     * @param string $batchRefNum
     *
     * @return array
     */
    public function getStatsForBatch(string $batchRefNum = null): array
    {
        $sql = "SELECT tr.card_type `card type`, COUNT(*) `count of transactions`, ROUND(SUM(tr.amount), 2) `total amount`
                  FROM transactions tr
                  JOIN batches b ON tr.b_ref_num = b.ref_num";

        if ($batchRefNum !== null) {
            $sql .= ' WHERE b.ref_num = ?';
        }

        $sql .= ' GROUP BY tr.card_type';
        /** @var PDO $stmt */
        $stmt = static::getPDO()->prepare($sql);
        $stmt->execute([$batchRefNum]);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Get stats for a merchant.
     *
     * @param string $merchantId
     * @param string $sDate
     * @param string $eDate
     *
     * @return array
     */
    public function getStatsForMerchant(string $merchantId, string $sDate, string $eDate): array
    {
        $sql = "SELECT tr.card_type `card type`, COUNT(*) `count of transactions`, ROUND(SUM(tr.amount), 2) `total amount`
                  FROM transactions tr
                  JOIN batches b ON tr.b_ref_num = b.ref_num
                  JOIN merchants m ON b.m_id = m.id
                  WHERE m.id = ? AND tr.date BETWEEN ? AND ?
                  GROUP BY tr.card_type";
        /** @var PDO $stmt */
        $stmt = static::getPDO()->prepare($sql);
        $stmt->execute([$merchantId, $sDate, $eDate]);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Get top merchants.
     *
     * @param int $limit
     * @param string $sDate
     * @param string $eDate
     *
     * @return array
     */
    public function getTopMerchants(int $limit, string $sDate, string $eDate): array
    {
        $sql = "SELECT m.id `merchant id`, m.name `merchant name`, ROUND(SUM(tr.amount), 2) `total amount`, COUNT(*) `count of transactions`
                  FROM transactions tr
                  JOIN batches b ON tr.b_ref_num = b.ref_num
                  JOIN merchants m ON b.m_id = m.id
                  WHERE tr.date BETWEEN :sdate and :edate
                  GROUP BY m.id
                  ORDER BY `total amount` DESC
                  LIMIT :limit";
        /** @var PDO $stmt */
        $stmt = static::getPDO()->prepare($sql);
        $stmt->bindParam(':limit', $limit, PDO::PARAM_INT);
        $stmt->bindParam(':sdate', $sDate);
        $stmt->bindParam(':edate', $eDate);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}
