<?php

require 'vendor/autoload.php';

// Init DB.
\Task\Iris\DB\DBInitialization::initDB();

// Handle report file.
\Task\Iris\ReportHandler::parseReport();

// SQL Queries.
$dbQuery = new \Task\Iris\DB\DBQuery();

try {
    // Display all transactions for a batch.
    $allBatchTransactions = $dbQuery->getAllTransactionsForBatch(
        '2264135688721936',
        '2018-05-04',
        '431731103030341346529798'
    );

    // Display stats for a batch.
    $statsForBatch = $dbQuery->getStatsForBatch('970788412093163424795');

    // Display stats for a merchant.
    $statsForMerchant = $dbQuery->getStatsForMerchant(
        '2264135688721936',
        '2018-05-04',
        '2018-05-06'
    );

    // Display top 10 merchants.
    $topMerchants = $dbQuery->getTopMerchants(
        10,
        '2018-05-04',
        '2018-05-06'
    );

} catch (Exception $e) {
    die($e->getMessage());
}