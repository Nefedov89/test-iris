<?php

declare(strict_types = 1);

namespace Task\Iris\Tests;

use PHPUnit\Framework\TestCase;
use Task\Iris\DB\DBQuery;
use Task\Iris\Traits\DBHelper;

/**
 * Class QueriesTest
 * @package Task\Iris\Tests
 */
final class QueriesTest extends TestCase
{
    use DBHelper;

    /**
     * @return void
     */
    public function testGetAllTransactionsForBatchQuery():void
    {
        $result = (new DBQuery())->getAllTransactionsForBatch(
            '2264135688721936',
            '2018-05-04',
            '431731103030341346529798'
        );

        $this->assertCount(2, $result);
    }

    /**
     * @return void
     */
    public function testGetStatsForBatchQuery():void
    {
        $result = (new DBQuery())->getStatsForBatch('970788412093163424795');

        $this->assertCount(4, $result);
    }

    /**
     * @return void
     */
    public function testGetStatsForMerchantQuery():void
    {
        $result1 = (new DBQuery())->getStatsForMerchant(
            '2264135688721936',
            '2018-05-04',
            '2018-05-06'
        );

        $result2 = (new DBQuery())->getStatsForMerchant(
            '2264135688721936',
            '2018-06-04',
            '2018-06-06'
        );

        $this->assertCount(4, $result1);
        $this->assertCount(0, $result2);
    }

    /**
     * @return void
     */
    public function testGetTopMerchantsQuery():void
    {
        $result1 = (new DBQuery())->getTopMerchants(
            10,
            '2018-05-04',
            '2018-05-06'
        );

        $result2 = (new DBQuery())->getTopMerchants(
            10,
            '2018-08-04',
            '2018-08-06'
        );

        $this->assertCount(10, $result1);
        $this->assertCount(0, $result2);
    }
}