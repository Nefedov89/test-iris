<?php

declare(strict_types = 1);

namespace Task\Iris\Tests;

use PHPUnit\Framework\TestCase;
use Task\Iris\ReportHandler;
use Task\Iris\Traits\DBHelper;

/**
 * Class CommonTest
 * @package Task\Iris\Tests
 */
final class CommonTest extends TestCase
{
    use DBHelper;

    /**
     * @return void
     */
    public function testReportFileExists(): void
    {
        $this->assertFileExists(ReportHandler::FILE_PATH);
    }

    /**
     * @return void
     */
    public function testReportFileIsReadable(): void
    {
        $this->assertFileIsReadable(ReportHandler::FILE_PATH);
    }
}