<?php

declare(strict_types = 1);

namespace Task\Iris\Tests;

use PDO;
use PHPUnit\Framework\TestCase;
use Task\Iris\Traits\DBHelper;

/**
 * Class DBTest
 * @package Task\Iris\Tests
 */
final class DBTest extends TestCase
{
    use DBHelper;

    /**
     * @return void
     */
    public function testInstanceOfDbConnection(): void
    {
        $this->assertInstanceOf(
            PDO::class,
            static::getPDO()
        );
    }

    /**
     * @return void
     */
    public function testMerchantsTableExists(): void
    {
        $this->assertTrue(static::checkIfTableExists('merchants'));
    }

    /**
     * @return void
     */
    public function testBatchesTableExists(): void
    {
        $this->assertTrue(static::checkIfTableExists('batches'));
    }

    /**
     * @return void
     */
    public function testTransactionsTableExists(): void
    {
        $this->assertTrue(static::checkIfTableExists('transactions'));
    }
}