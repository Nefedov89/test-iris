# Install

1. Run composer install
2. Set your DB credentials in DBConnection class
3. Run you php server (e.g. php -S localhost:5555)
4. Visit launched server in browser (after this all tables should be created and all data from report should be stored in db)
5. Run tests: phpunit tests
